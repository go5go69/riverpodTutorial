import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageWithStateful extends StatefulWidget {
  const PageWithStateful({Key? key}) : super(key: key);

  @override
  State<PageWithStateful> createState() => _PageWithStateful();
}

class _PageWithStateful extends State<PageWithStateful> {
  double _widthNumber = 0.0;
  double _heightNumber = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                color: CupertinoColors.white,
                child: Center(
                  child: Container(
                    width: _widthNumber,
                    height: _heightNumber,
                    decoration: const BoxDecoration(
                        image: DecorationImage(image: AssetImage('assets/images/seiya.jpg'))),
                  ),
                ),
              )),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Width: ${_widthNumber.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: _widthNumber,
                          onChanged: (value) {
                            setState(() {
                              _widthNumber = value;
                            });
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Height: ${_heightNumber.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: _heightNumber,
                          onChanged: (value) {
                            setState(() {
                              _heightNumber = value;
                            });
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}